if [ -f /usr/share/antigen.zsh ];then
    source /usr/share/antigen.zsh
else
    if [ ! -f $XDG_CONFIG_HOME/zsh/antigen.zsh ];then
        # coba ambil dari internet
        if ping -c 1 google.com >> /dev/null 2>&1; then
            curl -L git.io/antigen > $XDG_CONFIG_HOME/zsh/antigen.zsh
        fi
    fi
fi

[ -f $XDG_CONFIG_HOME/zsh/antigen.zsh ] && source $XDG_CONFIG_HOME/zsh/antigen.zsh

antigen use oh-my-zsh
antigen bundle pip                      # Python package manager autocomplete helper
antigen bundle common-aliases           # Common aliases like ll and la
antigen bundle djui/alias-tips          # Alias reminder when launching a command that is aliased
antigen bundle arialdomartini/oh-my-git # Cool git theme
if [ "$DISTRO" = "arch" ];then
    antigen bundle archlinux            # Aliases for pacman and yaourt (if installed)
fi
antigen bundle ssh-agent                # Launch ssh-agent
if [ "$DISTRO" = "lfs" ];then
    antigen bundle zsh-users/zsh-syntax-highlighting
fi
antigen bundle unixorn/autoupdate-antigen.zshplugin
antigen theme geometry-zsh/geometry
antigen apply
