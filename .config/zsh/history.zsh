# history
if [[ ! -d $XDG_CONFIG_HOME/zsh ]];then
    mkdir -p $XDG_CONFIG_HOME/zsh
fi

export HISTSIZE=2000
export HISTFILE="$XDG_CONFIG_HOME/zsh/.history"
export SAVEHIST=$HISTSIZE

setopt hist_ignore_all_dups
