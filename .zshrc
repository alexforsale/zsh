# set some stuffs first
export ZSH_CONFIG=$XDG_CONFIG_HOME/zsh
export ZSH_CACHE=$XDG_CACHE_HOME/zsh
export ZSH_LOCAL=$HOME/.local

# source some stuffs
if [[ -f /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]];then
. /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

if [[ -f /usr/share/doc/pkgfile/command-not-found.zsh ]];then
. /usr/share/doc/pkgfile/command-not-found.zsh
fi

typeset -ga sources
sources+="$ZSH_CONFIG/functions.zsh"
sources+="$ZSH_CONFIG/environment.zsh"
sources+="$ZSH_CONFIG/aliases.zsh"
sources+="$ZSH_CONFIG/history.zsh"
sources+="$ZSH_CONFIG/completions.zsh"
sources+="$ZSH_CONFIG/antigen.zsh"
sources+="$ZSH_CONFIG/antigenrc.zsh"

foreach file (`echo $sources`); do
    if [[ -e $file ]]; then
        source $file
    fi
done

# run the clock function
#if [[ "$TERM" != *linux* ]] || [[ "$TERM" != *screen* ]];then
#    clock && clear
#fi
